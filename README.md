# Summary
 
This demo case demonstrates wind load assessment on buildings using Computational Fluid Dynamics (CFD). Due to the large scale and complexity, such simulation needs High Performance Computing (HPC) resources to achieve timely and accurate results. The simulation is performed using the open-source software [OpenFOAM](https://openfoam.org). Due to the specific way of wind flow application, version 2.x of OpenFOAM should be used.
 
In this example case, wind flow over two neighboring highrise buildings is modeled. This nicely demonstrates how the environment can affect the wind load on a structure. Since wind is a stochastic process, it is necessary to simulate a longer period of time to obtain statistical values (mean, peak) that can be used for the design of structures.
 
The target audience for this demo case (mainly but not only) is structural engineering offices. Analytical wind load assessment methods are often not capable of being very conservative when designing structures of irregular shape or structures that are placed in unusual environments. The same is true for when designing elements (facade elements) where the wind load can be the decisive factor.
 
When assessing the wind loads for design purposes simulation for ~10 minutes is necessary. This test case, however, simulated the wind flow around the buildings for 60 seconds. The aim of this demo case is to demonstrate the workflow of such simulations as well as the capacities of the HPC. After familiarizing oneself with this case it is possible to get the feeling about what task needs to be performed for such simulations, how many resources are necessary and how much time does it take (also for the simulation duration of 10 minutes).
 
**This HPC demonstration was prepared as a part of the training and dissemination activities organized by the [EuroCC National HPC Competence Centre of Latvia](https://eurocc-latvia.lv/?lang=en).**
 
# Table of contents
 
[[_TOC_]]
 
# Problem description
 
This case demonstrates how HPC can be used for the assessment of wind loads on buildings. As an example, wind load on two high-rise buildings is simulated. The model setup follows general recommendations (requirements) for achieving reliable results:
- the length of the inflow field before the building should be more than 2 times the height of the building;
- the length of the domain after the building should be more than 5 times the height of the building;
- the height of the domain above the building should be at least 0.2 times the height of the building;
- the building should not take up more than 3% of the cross-section of the domain.
 
Following the recommendations and considering the size of the buildings (~30x75(h)m) the size of the domain is 550x300x200 meters.
The domain and mesh of the case are shown in the following images. With the current mesh refinement settings, the obtained mesh size is ~2.9 million elements.
 
![Python plot output](README_img/domain.png)
 
![Python plot output](README_img/domain_mesh.png)
 
The wind inflow is defined as time series of 3D velocity values at the pints of the inflow plane. The time series are obtained using a synthetic wind simulation library developed by Michael Andre [https://github.com/msandre/WindGen](url). 
 
# Contents of this case
 
An overview of all the files in this repository is below. You do not need to understand the meaning of each file in detail to run this simulation.
 
* `wind_sim` - OpenFOAM simulation files
    * `0` - Initial and boundary conditions for the domain.
    * `constant` - Geometry files, mesh definition, and other physical constants.
    * `system` -  The files that configure or help configure the case - a definition of solver, discretization schemes, geometry, parallelization, and other properties to obtain OpenFOAM solution.
* `mesh.pbs` - Script to mesh the simulation domain with the Portable Batch System
* `README.md` - This readme file
* `README_fig` - Images for this readme file
 
 
# Software prerequisites
 
* ***Git*** — To obtain this case directly from *GitLab* via the `clone` command. Alternatively, code can be downloaded as `.zip` or `.tar` archive and copied to the HPC cluster by other means.
* ***OpenFOAM 2.x*** — Used for the main simulation. 
* ***ParaVIEW*** (optional) — Can be used for examining the result `.vtk` files locally, after copying them off of the HPC cluster. Installers for your workstation can be found [here](https://www.paraview.org/download/).
 
 
# Obtaining the case
 
Download this case directly from GitLab by executing the command below. Alternatively, code can be downloaded as `.zip` or `.tar` archive and copied to the HPC cluster by other means.
```
git clone https://gitlab.com/eurocc-latvia/wind-loads.git
```
 
# Running the case
 
Simulation examples are designed so that pre-processing and simulations can be performed using OpenFOAM 2.x. Visualization of results can be done using Paraview.
 
Once you have obtained the case from the git repository, follow the steps described in this section to run the simulation.
 
## Meshing 
 
To mesh the domain, submit the batch job script by executing the command as seen below. _This script is adapted to run on [Riga Technical University HPC](https://hpc.rtu.lv/?lang=en) cluster._
```
qsub mesh.pbs
```
Subsections below describe the contents of the submitted job script and what happens when this job is being executed. 
 
### *mesh.pbs* script contents — Header
At the beginning of the `mesh.pbs` file are several lines that consider the resources allocated for this job. In this case, we allocate 8 CPU cores on one node, and we allocate these resources for 15 minutes.
```
#!/bin/sh
#PBS -N meshing
#PBS -q batch
#PBS -l walltime=00:15:00
#PBS -l nodes=1:ppn=8
#PBS -j oe
 
```
### *mesh.pbs* script contents — Loading modules
Next, the script will load the necessary modules, in this case, we only need one module that lets us use the OpenFOAM 2.4
```
module purge
module load openfoam/openfoam-2.4-centos7_test
 
```
### *mesh.pbs* script contents — Initializing OpenFOAM
The next command initializes OpenFOAM so that the rest of the commands can be executed.
```
foamInit
```
### *mesh.pbs* script contents — Meshing
The next sequence of commands does the meshing. First, we move into the directory where OpenFoam will find all the necessary information. Second, with _blockMesh_ the whole domain is meshed in background mesh using the sixe defined by the user in the file _constant/polyMesh/blockMeshDict_. After, the command _surfaceFeatureExtract_ extracts the information of the provides building geometry file _constant/triSurface/maja.stl_. Once that is done, the _decomposePar_ command splits the domain in several regions so that the meshing can be done in parallel. As next, the _snappyHexMesh_ command executes meshing in parallel on 8 CPU cores. Finally, the meshed domain is reassembled with the command _reconstructParMesh_. The output is written in out.log file. The mesh refinement definitions are located in file _system/snappyHexMeshDict_.
```
cd wind-loads/wind_sim/
 
blockMesh
 
surfaceFeatureExtract
 
decomposePar -force
 
##run snappyHexMesh
mpirun -np 8 snappyHexMesh -parallel > out.log
 
reconstructParMesh
```
Using the 8 CPU cores the meshing takes approximately 600 seconds and creates a mesh of ~2.9 million elements. One can check at the end of the execution (when the job has finished) in the out.log file if the meshing has been successful. The end of the file should have the following lines:
```
Finished meshing without any errors
Finished meshing in = 444.33 s.
End
 
Finalizing parallel run
```
 
## Running the simulation
 
Once meshing is done we can use the generated mesh to set up the simulation. To run the simulation, submit the batch job script by executing the command as seen below. _This script is adapted to run on [Riga Technical University HPC](https://hpc.rtu.lv/?lang=en) cluster._
```
qsub run.pbs
```
Subsections below describe the contents of the submitted job script and what happens when this job is being executed.
 
### *run.pbs* script contents — Beginning
The beginning of the _run.pbs_ file is similar to the one of _mesh.pbs_. The main difference is that more resources are requested. The simulation will be executed on 2 compute nodes (16 CPU cores each). And the estimated execution time is 8 hours. 
 
### *run.pbs* script contents — Preparing the run
The following commands assemble the _wind_sim_run_ directory using the mesh generated by the previous job.
```
cd wind-loads
 
rm -r wind_sim/processor*
 
cp -r wind_sim wind_sim_run
 
rm -r wind_sim_run/constant/polyMesh
 
mv wind_sim/0.02/polyMeshwind_sim_run/constant/.
 
cp add_files/controlDict_0to15 wind_sim_run/system/controlDict
 
cp add_files/decomposeParDict_run wind_sim_run/system/decomposeParDict
```
 
### *run.pbs* script contents — Running from 0 to 15 seconds
The simulation is run in two stages. The initial stage is run from 0 to 15 seconds. During the initial stage, a smaller time step is necessary to take care of the instabilities that may occur while the wind flow fills the domain. The necessary parameters (start and end time, time step, output frequency) are defined in _wind_sim_run/system/controlDict_. The domain is split into 32 sub-domains and the simulation is run using the pisoFoam solver in parallel. After reaching 15 seconds the domain is reassembled and the unnecessary sub-domain directories removed.
```
decomposePar -force
 
##run simulation from 0 to 15 seconds
mpirun -np 32 pisoFoam -parallel > out1.log
 
reconstructPar
 
rm -r processor*
```
 
### *run.pbs* script contents — Running from 15 to 60 seconds
The second stage is set to run from 15 to 60 seconds. In this stage, a larger time step can be used. The new _controlDict_ is copied to replace the file from the previous stage. The domain is split into 32 sub-domains and the simulation is run using the pisoFoam solver in parallel. After reaching 60 seconds the domain is reassembled and the unnecessary sub-domain directories removed. Finally, the result files are transformed into _.vtk_ files which can be used for post-processing the results using _Paraview_.
```
cp ../add_files/controlDict_15to60 system/controlDict
 
decomposePar -force
 
##run simulation from 15 to 60 seconds
mpirun -np 32 pisoFoam -parallel > out2.log
 
reconstructPar
 
rm -r processor*
 
foamToVTK
```
 
# Post-processing and visualization

Post-processing and visualization are done using the *ParaVIEW* utility. Download the `.vtk` files from the HPC cluster to interactively examine the solution. With the defined settings the OpenFoam will write the result files every 0.5 seconds.

The following images demonstrate post-processed results. For the velocity fields, the values are shown in _m/s_ and for the pressure distribution, the values correspond to the normalized pressure _p/rho_.

* animation of the wind velocity field in _xz_ plane
![Python plot output](README_img/y_cutplane.gif)
* animation of the wind velocity field in _xy_ plane
![Python plot output](README_img/z_cutplane.gif)
* average velocity field in _xz_ plane
![Python plot output](README_img/average_y_cutplane.png)
* average velocity field in _xy_ plane
![Python plot output](README_img/average_z_cutplane.png)
* average pressure on buildings
![Python plot output](README_img/average_pressure.png)
* maximal pressure on buildings
![Python plot output](README_img/max_pressure.png)
* minimal pressure on buildings
![Python plot output](README_img/min_pressure.png)
 
# Suggestions for altering the case
 
Two possible alterations for this case are suggested. The idea of the two alterations as well as the necessary steps is described in the following sections.
 
## Changing the mesh density
 
One simple alteration is to change the mesh density. This way the influence of mesh density on the results as well as on the execution time can be observed. Since the original mesh is considered to be fine enough for reliable results, here it is suggested to run the case with a coarser mesh.
 
In order to do that, the following command has to be executed before submitting the mesh.pbs job.
After cloning the case and before submitting the mesh.pbs job execute:
```
cp wind-loads/add_files/blockMeshDict_coarse wind-loads/wind_sim/constant/polyMesh/blockMeshDict
```
This command will replace the original _blockMeshDict_ file with the new one that defines the background mesh to have cells with double the edge length.
 
After the file has been relaced, proceed with the process as in the original scenario.
 
## Changing the orientation of the buildings
 
One of the tasks when assessing the wind loads on the buildings is to determine the critical direction. Usually, this is done with reduced models in order to determine the critical direction in a shorter period of time using fewer resources. The main simulation is done only for the critical direction(s).
 
To see the effect of wind attack angle a different building geometry file can be used for this case. 
In order to do that, the following command has to be executed before submitting the mesh.pbs job.
After cloning the case and before submitting the mesh.pbs job execute:
```
cp wind-loads/add_files/maja.stl wind-loads/wind_sim/constant/triSurface/.
```
This command will replace the original geometry file with the new one where the buildings have been rotated.
 
After the file has been relaced, proceed with the process as in the original scenario.
